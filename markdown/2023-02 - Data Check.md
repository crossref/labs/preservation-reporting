# Data Check: February 2023
This file documents the manual checking done against the preservation database in February 2023.

Records were extracted using the database's random-samples CLI command. For each archive, we checked 5 titles that should yield a positive result and 5 that should yield a negative. For each title, we checked a minimum of 2 DOIs, selected at random by hand. We also compared each title's records with The Keepers Registry to ensure that our records matched.

Variations in test outputs were a result of minor version increases during the testing process. For instance, v. 0.0.41 introduced a check against asserted preservation metadata.

Because different preservation systems present volume information in different formats, random-samples is not always able to determine, in one pass, which volumes are stored in a specific archive. Hence, for instance, PKP's PLN does not output volume information in its preservation claims. The same goes for OCUL Scholars Portal, where the volumes shown by random-samples do not reflect the total coverage. These records were manually verified against the Keepers Registry.

Records for HathiTrust status in sections other than HathiTrust may not be correct. Indeed, the records in each section should only be used to check the section in question as error corrections took place over the course of testing.  

## Archival Sources

| Archive                   | File                                                                            | Test Date  |
|---------------------------|---------------------------------------------------------------------------------|------------|
| Cariniana                 | http://reports-lockss.ibict.br/keepers/pln/ibictpln/keepers-IBICTPLN-report.csv | 2023-02-17 |
| CLOCKSS                   | https://reports.clockss.org/keepers/keepers-CLOCKSS-report.csv                  | 2023-02-16 |
| HathiTrust                | https://www.hathitrust.org/hathifiles                                           | 2023-02-23 |
| Internet Archive / FATCAT | https://archive.org/details/ia-keepers-registry-kbart                           | 2023-02    |
| LOCKSS                    | https://reports.lockss.org/keepers/keepers-LOCKSS-report.csv                    | 2023-02-20 |
| PKP PLN                   | https://pkp.sfu.ca/files/pkppn/onix.csv                                         | 2023-02-20 |
| Portico                   | https://api.portico.org/kbart/Portico_Holding_KBart.txt                         | 2023-02-21 |
| Scholars Portal           | Private data source. Please contact archive.                                    | 2023-02-21 |

## Archives

### Cariniana

#### Acta Scientiarum. Agronomy
Cariniana contains: Acta Scientiarum. Agronomy (issn: 1807-8621) (eissn: 1807-8621) (v1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18(1998; 1999; 2000; 2001; 2002; 2003; 2004; 2005; 2006; 2007; 2008; 2009; 2010; 2011; 2012; 2013; 2014; 2015)) (in progress: 29; 30; 31; 32; 33; 34; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45 (2007; 2008; 2009; 2010; 2011; 2012; 2013; 2014; 2015; 2016; 2017; 2018; 2019; 2020; 2021; 2022; 2023))  

DOI 1:

    [08:43:00] INFO     Checking 10.4025/actasciagron.v34i2.13143: ['Acta Scientiarum. Agronomy'] (['1807-8621', '1679-9275']) v34 n2
    [08:43:02] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal 

DOI 2:

    [08:45:37] INFO     Checking 10.4025/actasciagron.v38i4.30790: ['Acta Scientiarum. Agronomy'] (['1807-8621', '1679-9275']) v38 n4
    [08:45:39] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1807-8621).

---

#### Jornal Brasileiro de Pneumologia
Cariniana contains: Jornal Brasileiro de Pneumologia (issn: 1806-3713) (eissn: 1806-3756) (v()) (in progress: 30; 31; 32; 33; 34; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45; 46; 47; 48 (2004; 2005; 2006; 2007; 2008; 2009; 2010; 2011; 2012; 2013; 2014; 2015; 2016; 2017; 2018; 2019; 2020; 2021; 2022))                            

DOI 1:

    [08:56:56] INFO     Checking 10.1590/s1806-37132006000300003: ['Jornal Brasileiro de Pneumologia'] (['1806-3713']) v32 n3
    [08:56:58] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [08:59:23] INFO     Checking 10.1590/s1806-37132014000200002: ['Jornal Brasileiro de Pneumologia'] (['1806-3713']) v40 n2
    [08:59:24] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1806-3756).

---

#### Estudos Históricos (Rio de Janeiro)
Cariniana contains: Estudos Históricos (Rio de Janeiro) (issn: 0103-2186) (eissn: 2178-1494) (v()) (in progress: 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 32; 33; 34; 35 (2008; 2009; 2010; 2011; 2012; 2013; 2014; 2015; 2016; 2017; 2018; 2019; 2020; 2021; 2022)) 

DOI 1:

    [09:02:32] INFO     Checking 10.1590/s0103-21862009000200002: ['Estudos Históricos (Rio de Janeiro)'] (['0103-2186']) v22 n44
    [09:02:33] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [09:05:02] INFO     Checking 10.1590/s0103-21862014000200003: ['Estudos Históricos (Rio de Janeiro)'] (['0103-2186']) v27 n54
    [09:05:03] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2178-1494).

---

#### Revista Brasileira de Administração Científica
Cariniana contains: Revista Brasileira de Administração Científica (issn: ) (eissn: 2179-684X) (v1; 1; 2; 2; 3; 3; 4; 5(2010; 2010; 2011; 2011; 2012; 2012; 2013; 2014)) (in progress: 6; 7; 8; 9; 10; 11 (2015; 2016; 2017; 2018; 2019; 2020))

DOI 1:

    [09:09:18] INFO     Checking 10.6008/spc2179-684x.2016.001.0002: ['Revista Brasileira de Administração Científica'] (['2179-684X']) v7 n1
    [09:09:20] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [09:11:12] INFO     Checking 10.6008/spc2179-684x.2017.003.0005: ['Revista Brasileira de Administração Científica'] (['2179-684X']) v8 n3
    [09:11:14] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2179-684X).

---

#### Fórum Linguístico
Cariniana contains: Fórum Linguístico (issn: 1415-8698) (eissn: 1984-8412) (v1; 2; 3; 6; 10; 11; 12; 13; 14; 15; 16; 17; 18(1998; 1999; 2000; 2003; 2007; 2008; 2009; 2010; 2011; 2012; 2013; 2014; 2015)) (in progress: 19; 20; 21; 22; 23 (2016; 2017; 2018; 2019; 2020)) 

DOI 1:

    [09:16:02] INFO     Checking 10.5007/1984-8412.2021.e79653: ['Fórum Linguístico'] (['1984-8412', '1415-8698']) v18 nEsp.
    [09:16:05] INFO     Preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [09:19:06] INFO     Checking 10.5007/1984-8412.2023.e87147: ['Fórum Linguístico'] (['1984-8412', '1415-8698']) v20 n1
    [09:19:09] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1984-8412).

---

#### Letters in Mathematical Physics
Cariniana does not contain: Letters in Mathematical Physics (issn: 0377-9017) (eissn: 1573-0530)

DOI 1:

    [12:18:37] INFO     Checking 10.1007/s11005-022-01534-1: ['Letters in Mathematical Physics'] (['0377-9017', '1573-0530']) v112 n2
    [12:18:39] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [12:21:55] INFO     Checking 10.1007/s11005-016-0932-9: ['Letters in Mathematical Physics'] (['0377-9017', '1573-0530']) v107 n6
    [12:21:57] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1573-0530).

---

#### The International Journal of Entrepreneurship and Innovation
Cariniana does not contain: International Journal of Entrepreneurship and Innovation

DOI 1:

    [12:27:11] INFO     Checking 10.1177/1465750320974945: ['The International Journal of Entrepreneurship and Innovation'] (['1465-7503', '2043-6882']) v22 n3
    [12:27:13] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal 

DOI 2:

    [12:28:58] INFO     Checking 10.1177/1465750318796721: ['The International Journal of Entrepreneurship and Innovation'] (['1465-7503', '2043-6882']) v20 n3
    [12:29:00] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2043-6882).

---

#### International Journal of Society Systems Science
Cariniana does not contain: International Journal of Society Systems Science (issn: 1756-2511) (eissn: 1756-252X)

DOI 1:

    [12:32:05] INFO     Checking 10.1504/ijsss.2020.10034555: ['International Journal of Society Systems Science'] (['1756-2511', '1756-252X']) v12 n4
    [12:32:07] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:33:49] INFO     Checking 10.1504/ijsss.2011.043211: ['International Journal of Society Systems Science'] (['1756-2511', '1756-252X']) v3 n4
    [12:33:52] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1756-252X).

---

#### Journal of Teacher Education
Cariniana does not contain: Journal of Teacher Education (issn: 0022-4871) (eissn: 1552-7816)

DOI 1:

    [12:37:18] INFO     Checking 10.1177/0022487108331004: ['Journal of Teacher Education'] (['0022-4871', '1552-7816']) v60 n2
    [12:37:20] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [12:38:26] INFO     Checking 10.1177/002248716902000103: ['Journal of Teacher Education'] (['0022-4871', '1552-7816']) v20 n1
    [12:38:28] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1552-7816).

---

#### Cancer Translational Medicine
Cariniana does not contain: Cancer Translational Medicine (issn: 2395-3977) (eissn: 2395-3012)

DOI 1:

    [15:39:42] INFO     Checking 10.4103/ctm.ctm_31_19: ['Cancer Translational Medicine'] (['2395-3977']) v5 n4
    [15:39:43] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal 

DOI 2:

    [15:42:30] INFO     Checking 10.4103/2395-3977.189305: ['Cancer Translational Medicine'] (['2395-3977']) v2 n4
    [15:42:32] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2395-3012).

---

### CLOCKSS

#### Proceedings of the International Association of Hydrological Sciences
CLOCKSS contains: Proceedings of the International Association of Hydrological Sciences (issn: 2199-8981) (eissn: 2199-899X) (v364; 365; 366; 367; 368; 369; 370; 375(2014; 2015; 2015; 2015; 2015; 2015; 2015; 2017)) (in progress: 371; 372; 373; 374; 376; 377; 378; 379; 380; 381; 382; 383; 384 (2015; 2015; 2016; 2016; 2018; 2018; 2018; 2018; 2018; 2019; 2020; 2020; 2021))

DOI 1:

    [15:04:22] INFO     Checking 10.5194/piahs-364-3-2014: ['Proceedings of the International Association of Hydrological Sciences'] (['2199-899X']) v364 nNone
    [15:04:24] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [15:06:05] INFO     Checking 10.5194/piahs-369-1-2015: ['Proceedings of the International Association of Hydrological Sciences'] (['2199-899X']) v369 nNone
    [15:06:06] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 3 (in progress):

    [15:16:39] INFO     Checking 10.5194/piahs-380-1-2018: ['Proceedings of the International Association of Hydrological Sciences'] (['2199-899X']) v380 nNone
    [15:16:40] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
    [15:16:41] INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2199-899X).

---

#### Grazer Philosophische Studien
CLOCKSS contains: Grazer Philosophische Studien (issn: 0165-9227) (eissn: 1875-6735) (v1; 2; 3; 4; 5; 6; 7; 9; 10; 11; 12; 14; 15; 16; 18; 19; 20; 21; 22; 23; 24; 25; 27; 28; 29; 30; 31; 32; 33; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45; 46; 47; 48; 49; 50; 51; 52; 53; 54; 55; 56; 57; 58; 60(1975; 1976; 1977; 1977; 1978; 1978; 1979; 1979; 1980; 1980; 1981; 1981; 1982; 1982; 1982; 1983; 1983; 1984; 1984; 1985; 1985; 1985; 1986; 1986; 1987; 1987; 1988; 1988; 1989; 1989; 1989; 1990; 1990; 1991; 1991; 1991; 1992; 1992; 1993; 1993; 1993; 1994; 1994; 1994; 1995; 1996; 1996; 1997; 1998; 1998; 1998; 1999; 2000; 2000)) (in progress:  ())

DOI 1:

    [15:17:58] INFO     Checking 10.5840/gps2000609: ['Grazer Philosophische Studien'] (['0165-9227']) v60 nNone
    [15:17:59] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [15:21:00] INFO     Checking 10.5840/gps1984227: ['Grazer Philosophische Studien'] (['0165-9227']) v22 nNone
    [15:21:01] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal 

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1875-6735).

---

#### Hellenic Journal of Cardiology
CLOCKSS contains: Hellenic Journal of Cardiology (issn: 1109-9666) (eissn: ) (v57; 58; 57; 58; 59; 60; 58; 59; 60; 61; 59; 60; 61; 62; 60; 61; 62; 61; 62; 63; 64; 63; 64; 65; 66; 67; 68; 69; 69(2016; 2016; 2017; 2017; 2017; 2017; 2018; 2018; 2018; 2018; 2019; 2019; 2019; 2019; 2020; 2020; 2020; 2021; 2021; 2021; 2021; 2022; 2022; 2022; 2022; 2022; 2022; 2022; 2023)) (in progress:  ())

DOI 1:

    [15:39:55] INFO     Checking 10.1016/j.hjc.2017.11.012: ['Hellenic Journal of Cardiology'] (['1109-9666']) v58 n5
    [15:39:56] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [15:42:10] INFO     Checking 10.1016/j.hjc.2016.04.002: ['Hellenic Journal of Cardiology'] (['1109-9666']) v57 n2
    [15:42:12] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2241-5955).

---

#### Interface Focus

CLOCKSS contains: Interface Focus (issn: 2042-8898) (eissn: 2042-8901) (v1; 2; 3; 4; 5; 6; 7; 8; 9(2011; 2012; 2013; 2014; 2015; 2016; 2017; 2018; 2019)) (in progress: 10; 11; 12 (2020; 2021; 2022))

DOI 1:

    [16:48:10] INFO     Checking 10.1098/rsfs.2016.0113: ['Interface Focus'] (['2042-8898', '2042-8901']) v7 n4
    [16:48:12] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2 (in progress):

    [16:49:41] INFO     Checking 10.1098/rsfs.2021.0066: ['Interface Focus'] (['2042-8901']) v12 n2
    [16:49:43] INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2042-8901).

---

#### Brazilian Journal of Geology

DOI 1:

    [16:56:37] INFO     Checking 10.1590/23174889201500020006: ['Brazilian Journal of Geology'] (['2317-4889']) v45 n2
    [16:56:39] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [16:59:36] INFO     Checking 10.5327/z2317-4889201400020011: ['Brazilian Journal of Geology'] (['2317-4889']) v44 n2
    [16:59:38] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2317-4692).

---

#### Revista Ibero-Americana de Estudos em Educação

CLOCKSS does not contain: Revista Ibero-Americana de Estudos em Educação (issn: 2446-8606) (eissn: 1982-5587)

DOI 1:

    [17:09:52] INFO     Checking 10.21723/riaee.v9i2.7036: ['Revista Ibero-Americana de Estudos em Educação'] (['1982-5587']) v9 n2
    [17:09:54] INFO     Preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [17:13:24] INFO     Checking 10.21723/riaee.v4i3.2768: ['Revista Ibero-Americana de Estudos em Educação'] (['1982-5587']) v4 n3
    [17:13:25] INFO     Preserved: in Cariniana
    [17:13:26] INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1982-5587).

---

#### Brazilian Journal of Medicine and Human Health
CLOCKSS does not contain: Brazilian Journal of Medicine and Human Health (issn: 2317-3386)

DOI 1:

    [17:58:11] INFO     Checking 10.17267/2317-3386bjmhh.v2i1.326: ['Brazilian Journal of Medicine and Human Health'] (['2317-3386']) v2 n1
    [17:58:13] INFO     Preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2 (in progress in Cariniana):

    [17:59:30] INFO     Checking 10.17267/2317-3386bjmhh.v5i1.1275: ['Brazilian Journal of Medicine and Human Health'] (['2317-3386']) v5 n1
    [17:59:32] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2317-3386).

---

#### Signos
CLOCKSS does not contain: Signos (issn: 1413-0416) (eissn: 1983-0378)

DOI 1 (in progress in Cariniana):

    [18:03:36] INFO     Checking 10.22410/issn.1983-0378.v41i1a2020.2532: ['Revista Signos'] (['1983-0378']) v41 n1
    [18:03:38] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2 (in progress in Cariniana):

    [18:05:24] INFO     Checking 10.22410/issn.1983-0378.v38i1a2017.1356: ['Revista Signos'] (['1983-0378']) v38 n1
    [18:05:26] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1983-0378).

---

#### Alea: Estudos Neolatinos
CLOCKSS does not contain: Alea: Estudos Neolatinos (issn: 1517-106X) (eissn: 1807-0299)

DOI 1 (in progress in Cariniana):

    [18:08:36] INFO     Checking 10.1590/s1517-106x2011000200007: ['Alea : Estudos Neolatinos'] (['1517-106X']) v13 n2
    [18:08:37] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
    [18:08:38] INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2 (in progress in Cariniana):

    [18:10:13] INFO     Checking 10.1590/s1517-106x2003000100002: ['Alea : Estudos Neolatinos'] (['1517-106X']) v5 n1
    [18:10:15] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal 

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1807-0299).

---

#### Revista Subjetividades
CLOCKSS does not contain: Subjetividades (issn: ) (eissn: 2359-0777)

DOI 1 (in progress in Cariniana and in PKP PLN):

    [18:25:15] INFO     Checking 10.5020/23590777.rs.v20i1.e9778: ['Revista Subjetividades'] (['2359-0777', '2359-0769']) v20 n1
    [18:25:18] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2 (in progress in Cariniana and in):

    [18:27:45] INFO     Checking 10.5020/23590777.15.3.375-388: ['Revista Subjetividades'] (['2359-0769', '2359-0777']) v15 n3
    [18:27:48] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2359-0777).

---

### HathiTrust

#### Russian Studies in Literature 

HathiTrust contains Russian studies in literature (issn: 1061-1975) (v[32])

DOI 1:

    [11:13:17] INFO     Checking 10.2753/rsl1061-1975320361: ['Russian Studies in Literature'] ({'1944-7167', '1061-1975'}) v32 n3
    [11:13:20] INFO     10.2753/RSL1061-1975320361 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [11:35:41] INFO     Checking 10.2753/rsl1061-1975460202: ['Russian Studies in Literature'] ({'1944-7167', '1061-1975'}) v46 n2
    [11:35:44] INFO     10.2753/RSL1061-1975460202 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records do not correspond](https://portal.issn.org/resource/ISSN/1944-7167). But [HathiTrust catalogue](https://catalog.hathitrust.org/Record/002587977?type%5B%5D=isn&lookfor%5B%5D=1061-1975&ft=) confirms presence.

---

#### The Journal of Egyptian Archaeology
HathiTrust contains The journal of Egyptian archaeology (issn: 0307-5133) (v[79])

DOI 1:

    [11:46:09] INFO     Checking 10.1177/030751339908500108: ['The Journal of Egyptian Archaeology'] ({'0307-5133', '2514-0582'}) v85 n1
    [11:46:11] INFO     10.1177/030751339908500108 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [11:49:33] INFO     Checking 10.1177/030751337506100104: ['The Journal of Egyptian Archaeology'] ({'2514-0582', '0307-5133'}) v61 n1
    [11:49:36] INFO     10.1177/030751337506100104 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records do not correspond](https://portal.issn.org/resource/ISSN/2514-0582). But [HathiTrust catalogue](https://catalog.hathitrust.org/Search/Home?lookfor=0307-5133&searchtype=isn&ft=ft&setft=true) confirms presence.

---

#### Clinical Rehabilitation
HathiTrust contains Clinical rehabilitation (issn: 0269-2155) (v[6])

DOI 1:

    [12:01:48] INFO     Checking 10.1177/026921559200600303: ['Clinical Rehabilitation'] ({'0269-2155', '1477-0873'}) v6 n3
    [12:01:51] INFO     10.1177/026921559200600303 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [12:03:22] INFO     Checking 10.1177/026921559200600203: ['Clinical Rehabilitation'] ({'0269-2155', '1477-0873'}) v6 n2
    [12:03:25] INFO     10.1177/026921559200600203 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records do not correspond](https://portal.issn.org/resource/ISSN/1477-0873). But [HathiTrust catalogue](https://catalog.hathitrust.org/Record/000888414?type%5B%5D=isn&lookfor%5B%5D=0269-2155&ft=) confirms presence.

---

#### Journal of AOAC INTERNATIONAL
HathiTrust contains Journal of the Association of Official Analytical Chemists (issn: 0004-5756) (v[57])

DOI 1:

    [12:07:39] INFO     Checking 10.1093/jaoac/3.2.193: ['Journal of AOAC INTERNATIONAL'] ({'0095-9111'}) v3 n2
    [12:07:41] INFO     10.1093/jaoac/3.2.193 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:09:02] INFO     Checking 10.1093/jaoac/3.2.276: ['Journal of AOAC INTERNATIONAL'] ({'0095-9111'}) v3 n2
    [12:09:04] INFO     10.1093/jaoac/3.2.276 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/0004-5756).

---

#### Veritas

HathiTrust contains Veritas (issn: 0042-3955) (v[165, 166, 167, 168])

DOI 1:

    [12:32:10] INFO     Checking 10.15448/1984-6746.2005.4.1818: ['Veritas (Porto Alegre)'] ({'1984-6746', '0042-3955'}) v50 n4
    [12:32:13] INFO     10.15448/1984-6746.2005.4.1818 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:33:53] INFO     Checking 10.15448/1984-6746.2007.3.4676: ['Veritas (Porto Alegre)'] ({'1984-6746', '0042-3955'}) v52 n3
    [12:33:57] INFO     10.15448/1984-6746.2007.3.4676 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records do not correspond](https://portal.issn.org/resource/ISSN/1984-6746). But [HathiTrust catalogue](https://catalog.hathitrust.org/Record/003708244?type%5B%5D=isn&lookfor%5B%5D=0042-3955&ft=) confirms presence.

---
#### Journal of the Brazilian Society of Mechanical Sciences and Engineering
HathiTrust does not contain: Journal of the Brazilian Society of Mechanical Sciences and Engineering (issn: 1678-5878) (eissn: 1806-3691)

DOI 1:

    [15:39:58] INFO     Checking 10.1007/s40430-017-0717-9: ['Journal of the Brazilian Society of Mechanical Sciences and Engineering'] ({'1678-5878', '1806-3691'}) v39 n8
    [15:40:00] INFO     10.1007/s40430-017-0717-9 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [15:41:14] INFO     Checking 10.1007/s40430-015-0368-7: ['Journal of the Brazilian Society of Mechanical Sciences and Engineering'] ({'1678-5878', '1806-3691'}) v38 n3
    [15:41:17] INFO     10.1007/s40430-015-0368-7 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1806-3691).

---

#### Revista de Ciências Exatas e Tecnologia
HathiTrust does not contain: Revista de Ciências Exatas e Tecnologia (issn: 1980-1793) (eissn: 2178-6895)

DOI 1:

    [15:42:57] INFO     Checking 10.17921/1890-1793.2018v13n13p09-12: ['Revista de Ciências Exatas e Tecnologia'] ({'1980-1793', '2178-6895'}) v13 n13
    [15:43:01] INFO     10.17921/1890-1793.2018v13n13p09-12 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [15:45:24] INFO     Checking 10.17921/1890-1793.2016v11n11p23-27: ['Revista de Ciências Exatas e Tecnologia'] ({'1980-1793', '2178-6895'}) v11 n11
    [15:45:28] INFO     10.17921/1890-1793.2016v11n11p23-27 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2178-6895).

---

#### Dilemas: Revista de Estudos de Conflito e Controle Social
HathiTrust does not contain: Dilemas: Revista de Estudos de Conflito e Controle Social (issn: 2178-2792) (eissn: 2178-2792)

DOI 1:

    [15:48:31] INFO     Checking 10.17648/dilemas.v13n3.32082: ['Dilemas - Revista de Estudos de Conflito e Controle Social'] ({'2178-2792', '1983-5922'}) v13 n3
    [15:48:35] INFO     10.17648/dilemas.v13n3.32082 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [15:50:42] INFO     Checking 10.17648/dilemas.v14n2.31761: ['Dilemas - Revista de Estudos de Conflito e Controle Social'] ({'1983-5922', '2178-2792'}) v14 n2
    [15:50:45] INFO     10.17648/dilemas.v14n2.31761 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2178-2792).

---

#### Revista de Cultura Teológic
HathiTrust does not contain: Revista de Cultura Teológic (issn: 0104-0529) (eissn: 2317-4307)

DOI 1:

    [16:27:05] INFO     Checking 10.23925/rct.i99.54231: ['Revista de Cultura Teológica'] ({'2317-4307', '0104-0529'}) vNone n99
    [16:27:08] INFO     10.23925/rct.i99.54231 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [16:31:26] INFO     Checking 10.23925/rct.i103.59569: ['Revista de Cultura Teológica'] ({'2317-4307', '0104-0529'}) vNone n103
    [16:31:30] INFO     10.23925/rct.i103.59569 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2317-4307). Note that this title does not accurately lodge correct volume metadata.

---

#### Boletim Paranaense de Geociências
HathiTrust does not contain: Boletim Paranaense de Geociências (issn: 0067-964X) (eissn: )

DOI 1:

    [16:47:33] INFO     Checking 10.5380/geo.v74i1.54132: ['Boletim Paranaense de Geociências'] ({'0067-964X'}) v74 n1
    [16:47:35] INFO     10.5380/geo.v74i1.54132 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [16:49:17] INFO     Checking 10.5380/geo.v70i0.31555: ['Boletim Paranaense de Geociências'] ({'0067-964X'}) v70 nNone
    [16:49:19] INFO     10.5380/geo.v70i0.31555 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/0067-964X).

---

### LOCKSS

#### Evolutionary Bioinformatics
LOCKSS contains: Evolutionary Bioinformatics (issn: ) (eissn: 1176-9343) (v7; 8; 9; 10; 11(2011; 2012; 2013; 2014; 2015)) (in progress:  ())

DOI 1:

    [09:35:00] INFO     Checking 10.4137/ebo.s10372: ['Evolutionary Bioinformatics'] (['1176-9343', '1176-9343']) v8 nNone
               INFO     10.4137/EBO.S10372 asserts no preservation information.
    [09:35:03] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [09:36:50] INFO     Checking 10.4137/ebo.s11169: ['Evolutionary Bioinformatics'] (['1176-9343', '1176-9343']) v9 nNone
               INFO     10.4137/EBO.S11169 asserts no preservation information.
    [09:36:52] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1176-9343).

---

#### Applied Biochemistry and Microbiology
LOCKSS contains: Applied Biochemistry and Microbiology (issn: 0003-6838) (eissn: 1608-3024) (v49; 50; 51; 52; 53; 54(2013; 2014; 2015; 2016; 2017; 2018)) (in progress: 48; 55 (2012; 2019))

DOI 1:

    [09:43:04] INFO     Checking 10.1134/s0003683817060126: ['Applied Biochemistry and Microbiology'] (['0003-6838', '1608-3024']) v53 n6
               INFO     10.1134/S0003683817060126 asserts no preservation information.
    [09:43:06] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2 (in progress):

    [09:46:29] INFO     Checking 10.1134/s0003683819050077: ['Applied Biochemistry and Microbiology'] (['0003-6838', '1608-3024']) v55 n5
               INFO     10.1134/S0003683819050077 asserts no preservation information.
    [09:46:31] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1608-3024).

---

#### Neurogenesis
LOCKSS contains: Neurogenesis (issn: ) (eissn: 2326-2133) (v1; 2; 3; 4; 5(2014; 2015; 2016; 2017; 2018)) (in progress:  ())

DOI 1:

    [10:17:22] INFO     Checking 10.1080/23262133.2016.1242455: ['Neurogenesis'] (['2326-2133']) v3 n1
               INFO     10.1080/23262133.2016.1242455 asserts no preservation information.
    [10:17:24] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [10:19:43] INFO     Checking 10.1080/23262133.2016.1262934: ['Neurogenesis'] (['2326-2133']) v4 n1
               INFO     10.1080/23262133.2016.1262934 asserts no preservation information.
    [10:19:45] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2326-2133).

---

#### Intensive Care Medicine Experimental
LOCKSS contains: Intensive Care Medicine Experimental (issn: ) (eissn: 2197-425X) (v4; 5; 6(2016; 2017; 2018)) (in progress: 2; 3; 7 (2014; 2015; 2019))

DOI 1:

    [10:26:18] INFO     Checking 10.1186/s40635-016-0112-3: ['Intensive Care Medicine Experimental'] (['2197-425X']) v4 n1
               INFO     10.1186/s40635-016-0112-3 asserts no preservation information.
    [10:26:20] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal 

DOI 2:

    [10:30:18] INFO     Checking 10.1186/s40635-017-0163-0: ['Intensive Care Medicine Experimental'] (['2197-425X']) v5 n1
               INFO     10.1186/s40635-017-0163-0 asserts no preservation information.
    [10:30:20] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2197-425X).

---

#### Soil Mechanics and Foundation Engineering
LOCKSS contains: Soil Mechanics and Foundation Engineering (issn: 0038-0741) (eissn: 1573-9279) (v50; 51; 52; 53; 54; 55(2013; 2014; 2015; 2016; 2017; 2018)) (in progress: 49; 56 (2012; 2019))

DOI 1:

    [10:37:06] INFO     Checking 10.1007/s11204-014-9258-8: ['Soil Mechanics and Foundation Engineering'] (['0038-0741', '1573-9279']) v51 n2
               INFO     10.1007/s11204-014-9258-8 asserts no preservation information.
    [10:37:07] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [10:38:40] INFO     Checking 10.1007/s11204-013-9213-0: ['Soil Mechanics and Foundation Engineering'] (['0038-0741', '1573-9279']) v50 n2
               INFO     10.1007/s11204-013-9213-0 asserts no preservation information.
    [10:38:42] INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1573-9279).

---

#### Materials Research
LOCKSS does not contain: Materials Research (issn: 1516-1439) (eissn: 1980-5373)

DOI 1:

    [10:49:55] INFO     Checking 10.1590/s1516-14392014005000072: ['Materials Research'] (['1980-5373', '1516-1439']) v17 n3
               INFO     10.1590/S1516-14392014005000072 asserts no preservation information.
    [10:49:58] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [10:51:50] INFO     Checking 10.1590/s1516-14391999000200002: ['Materials Research'] (['1516-1439']) v2 n2
    [10:51:51] INFO     10.1590/S1516-14391999000200002 asserts no preservation information.
    [10:51:52] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1980-5373).

---

#### Revista Kinesis
LOCKSS does not contain: Revista Kinesis (issn: 0102-8308) (eissn: 2316-5464)

DOI 1:

    [10:59:36] INFO     Checking 10.5902/231654648244: ['Kinesis'] (['2316-5464', '0102-8308']) v30 n2
               INFO     10.5902/231654648244 asserts no preservation information.
    [10:59:39] INFO     Preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:04:45] INFO     Checking 10.5902/010283085717: ['Kinesis'] (['2316-5464', '0102-8308']) v30 n1
               INFO     10.5902/010283085717 asserts no preservation information.
    [11:04:48] INFO     Preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2316-5464).

---

#### Revista Aspas
LOCKSS does not contain: Revista Aspas (issn: ) (eissn: 2238-3999)

DOI 1:

    [11:08:20] INFO     Checking 10.11606/issn.2238-3999.v9i2p23-44: ['Revista Aspas'] (['2238-3999']) v9 n2
               INFO     10.11606/issn.2238-3999.v9i2p23-44 asserts no preservation information.
    [11:08:22] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:10:23] INFO     Checking 10.11606/issn.2238-3999.v5i2p90-102: ['Revista aSPAs'] (['2238-3999']) v5 n2
               INFO     10.11606/issn.2238-3999.v5i2p90-102 asserts no preservation information.
    [11:10:25] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2238-3999).

---

#### Ideias
LOCKSS does not contain: Ideias (issn: 0104-7876) (eissn: 2179-5525)

DOI 1:

    [11:12:48] INFO     Checking 10.20396/ideias.v9i1.8652788: ['Idéias'] (['2179-5525']) v9 n1
               INFO     10.20396/ideias.v9i1.8652788 asserts no preservation information.
    [11:12:49] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:14:38] INFO     Checking 10.20396/ideias.v5i2.8649434: ['Idéias'] (['2179-5525']) v5 n2
               INFO     10.20396/ideias.v5i2.8649434 asserts no preservation information.
    [11:14:39] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2179-5525).

---

#### Zoologia (Curitiba)
LOCKSS does not contain: Zoologia (Curitiba) (issn: 1984-4670) (eissn: 1984-4689)

DOI 1:

    [11:17:07] INFO     Checking 10.1590/s1984-46702012000300003: ['Zoologia (Curitiba)'] (['1984-4689']) v29 n3
               INFO     10.1590/S1984-46702012000300003 asserts no preservation information.
    [11:17:09] INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:20:14] INFO     Checking 10.1590/s1984-46702013000200001: ['Zoologia (Curitiba)'] (['1984-4689']) v30 n2
               INFO     10.1590/S1984-46702013000200001 asserts no preservation information.
    [11:20:16] INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1984-4689).

---

### PKP Private LOCKSS Network

#### Ius Canonicum
PKP PLN contains: Ius Canonicum (issn: 2254-6219)

DOI 1:

    [11:36:03] INFO     Checking 10.15581/016.41.15069: ['Ius Canonicum'] (['2254-6219', '0021-325X']) v41 n82
               INFO     10.15581/016.41.15069 asserts no preservation information.
    [11:36:06] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:51:38] INFO     Checking 10.15581/016.38.15893: ['Ius Canonicum'] (['2254-6219', '0021-325X']) v38 n76
               INFO     10.15581/016.38.15893 asserts no preservation information.
    [11:51:41] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2254-6219).

---

#### Journal of Islamic Thought and Civilization
PKP PLN contains: Journal of Islamic Thought and Civilization (issn: 2520-0313)

DOI 1:

    [11:57:48] INFO     Checking 10.32350/jitc.111.01: ['Journal of Islamic Thought and Civilization'] (['2520-0313', '2075-0943']) v11 n1
               INFO     10.32350/jitc.111.01 asserts no preservation information.
    [11:57:51] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:03:10] INFO     Checking 10.32350/jitc.121.08: ['Journal of Islamic Thought and Civilization'] (['2520-0313', '2075-0943']) v12 n1
               INFO     10.32350/jitc.121.08 asserts no preservation information.
    [12:03:12] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2520-0313).

---

#### Revista de Humanidades Digitales
PKP PLN contains: Revista de Humanidades Digitales (issn: 2531-1786)

DOI 1:

    [12:09:24] INFO     Checking 10.5944/rhd.vol.1.2017.16683: ['Revista de Humanidades Digitales'] (['2531-1786']) v1 n0
               INFO     10.5944/rhd.vol.1.2017.16683 asserts no preservation information.
    [12:09:26] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:10:27] INFO     Checking 10.5944/rhd.vol.3.2019.23188: ['Revista de Humanidades Digitales'] (['2531-1786']) v3 nNone
               INFO     10.5944/rhd.vol.3.2019.23188 asserts no preservation information.
    [12:10:29] INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2531-1786).

---

#### International Journal Online of Humanities
PKP PLN contains: International Journal Online of Humanities (issn: 2395-5155)

DOI 1:

    [12:32:03] INFO     Checking 10.24113/ijohmn.v6i6.210: ['IJOHMN (International Journal online of Humanities)'] (['2395-5155']) vNone nNone
    [12:32:05] INFO     10.24113/ijohmn.v6i6.210 incorrectly asserts preservation in LOCKSS.
               INFO     10.24113/ijohmn.v6i6.210 incorrectly asserts preservation in Portico.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Note: this DOI incorrectly asserts preservation in Portico. Keepers Registry reports that Portico only preserves up to 2019. It also incorrectly asserts that LOCKSS preserves this content, when referring to PKP's Private LOCKSS Network.

DOI 2:

    [12:38:54] INFO     Checking 10.24113/ijohmn.v4i3.48: ['IJOHMN (International Journal online of Humanities)'] (['2395-5155']) v4 n3
    [12:38:56] INFO     10.24113/ijohmn.v4i3.48 correctly asserts preservation in Portico.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2395-5155). Records do not correspond to member assertions.

---

#### Didacticae: Journal of Research in Specific Didactics
PKP PLN contains: Didacticae: Journal of Research in Specific Didactics (issn: 2462-2737)

DOI 1:

    [13:03:44] INFO     Checking 10.1344/did.2019.6.37-52: ['Didacticae'] (['2462-2737']) vNone nNone
    [13:03:45] INFO     10.1344/did.2019.6.37-52 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [13:07:06] INFO     Checking 10.1344/did.2017.1.77-96: ['Didacticae'] (['2462-2737']) vNone n2
    [13:07:08] INFO     10.1344/did.2017.1.77-96 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2462-2737).

---

#### Revista Museologia & Interdisciplinaridade
PKP PLN does not contain: Revista Museologia & Interdisciplinaridade

DOI 1:

    [14:02:17] INFO     Checking 10.26512/museologia.v7i13.17776: ['Museologia &amp; Interdisciplinaridade'] (['2238-5436']) v7 n13
    [14:02:19] INFO     10.26512/museologia.v7i13.17776 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [14:03:37] INFO     Checking 10.26512/museologia.v5i9.17245: ['Museologia &amp; Interdisciplinaridade'] (['2238-5436']) v5 n9
    [14:03:38] INFO     10.26512/museologia.v5i9.17245 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
    [14:03:39] INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2238-5436).

---

#### Crop Breeding and Applied Biotechnology
PKP PLN does not contain: Crop Breeding and Applied Biotechnology (issn: 1984-7033) (eissn: 1984-7033)

DOI 1:

    [14:06:28] INFO     Checking 10.1590/1984-70332014v14n3a21: ['Crop Breeding and Applied Biotechnology'] (['1984-7033']) v14 n3
    [14:06:30] INFO     10.1590/1984-70332014v14n3a21 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [14:07:57] INFO     Checking 10.1590/1984-70332016v16n4a45: ['Crop Breeding and Applied Biotechnology'] (['1984-7033']) v16 n4
    [14:07:59] INFO     10.1590/1984-70332016v16n4a45 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1984-7033).

---

#### Dental Press Journal of Orthodontics
PKP PLN does not contain: Dental Press Journal of Orthodontics (issn: 2176-9451) (eissn: 2177-6709)

DOI 1:

    [14:14:19] INFO     Checking 10.1590/2176-9451.19.4.018-026.oin: ['Dental Press Journal of Orthodontics'] (['2176-9451']) v19 n4
    [14:14:21] INFO     10.1590/2176-9451.19.4.018-026.oin asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [14:15:36] INFO     Checking 10.1590/2177-6709.21.5.039-046.oar: ['Dental Press Journal of Orthodontics'] (['2176-9451']) v21 n5
    [14:15:37] INFO     10.1590/2177-6709.21.5.039-046.oar asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
    [14:15:38] INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2177-6709).

---

#### Brazilian Journal of Political Economy
PKP PLN does not contain: Brazilian Journal of Political Economy (issn: 0101-3157) (eissn: 1809-4538)

DOI 1:

    [14:34:41] INFO     Checking 10.1590/0101-31572015v35n02a03: ['Revista de Economia Política'] (['0101-3157']) v35 n2
    [14:34:42] INFO     10.1590/0101-31572015v35n02a03 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [14:35:42] INFO     Checking 10.1590/0101-31572016v37n01a03: ['Revista de Economia Política'] (['0101-3157']) v37 n1
    [14:35:44] INFO     10.1590/0101-31572016v37n01a03 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1809-4538).

---

#### Sociologias
PKP PLN does not contain: Sociologias (issn: 1517-4522) (eissn: 1807-0337)

DOI 1:

    [14:58:02] INFO     Checking 10.1590/15174522-017003903: ['Sociologias'] (['1517-4522']) v17 n39
    [14:58:04] INFO     10.1590/15174522-017003903 asserts no preservation information.
               INFO     Preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [14:59:03] INFO     Checking 10.1590/15174522-019004511: ['Sociologias'] (['1517-4522']) v19 n45
    [14:59:05] INFO     10.1590/15174522-019004511 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1807-0337).

---

### Portico

#### IET Electric Power Applications
Portico contains: IET Electric Power Applications (issn: 1751-8660) (eissn: 1751-8679) (v2013 - v. 7 (1-9), 2014 - v. 8 (1-9), 2015 - v. 9 (1-9), 2016 - v. 10 (1-9), 2017 - v. 11 (1-9), 2018 - v. 12 (1-9), 2019 - v. 13 (1-12), 2020 - v. 14 (1-14), 2021 - v. 15 (1-12), 2022 - v. 16 (1-12), 2023 - v. 17 (1))

DOI 1:

    [11:25:11] INFO     Checking 10.1049/iet-epa.2013.0405: ['IET Electric Power Applications'] ({'1751-8679', '1751-8660'}) v8 n8
    [11:25:13] INFO     10.1049/iet-epa.2013.0405 correctly asserts preservation in Portico.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico

DOI 2:

    [11:26:28] INFO     Checking 10.1049/elp2.12190: ['IET Electric Power Applications'] ({'1751-8660', '1751-8679'}) v16 n7
    [11:26:31] INFO     10.1049/elp2.12190 correctly asserts preservation in Portico.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1751-8679).

---

#### Indian Journal of Thoracic and Cardiovascular Surgery
Portico contains: Indian Journal of Thoracic and Cardiovascular Surgery (issn: 0970-9134) (eissn: 0973-7723) (v2000 - v. 16 (1-2), 2001 - v. 17 (1-4), 2002 - v. 18 (1-4), 2003 - v. 19 (1-4), 2004 - v. 20 (1-4), 2005 - v. 21 (1-4), 2006 - v. 22 (1-4), 2007 - v. 23 (1-4), 2008 - v. 24 (1-4), 2009/2010 - v. 25 (1-4), 2010 - v. 26 (1-4), 2011 - v. 27 (1-4), 2012 - v. 28 (1-4), 2013 - v. 29 (1-4), 2014 - v. 30 (1-3), 2015 - v. 31 (1-4), 2016 - v. 32 (1-4), 2017 - v. 33 (1-4), 2018 - v. 34 (1, Supplement 1, 2, Supplement 2, 3-4), 2019 - v. 35 (1-4), 2019/2020 - v. 36 (1, Supplement 1, 2, Supplement 2, 3-6), 2021 - v. 37 (1, Supplement 1, 2, Supplement 2, 3, Supplement 3, 4-6), 2022 - v. 38 (1, Supplement 1, 2, Supplement 2, 3-6))

DOI 1:

    [11:30:16] INFO     Checking 10.1007/s12055-018-0726-z: ['Indian Journal of Thoracic and Cardiovascular Surgery'] ({'0973-7723', '0970-9134'}) v34 nS3
    [11:30:18] INFO     10.1007/s12055-018-0726-z asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [11:31:55] INFO     Checking 10.1007/s12055-012-0157-1: ['Indian Journal of Thoracic and Cardiovascular Surgery'] ({'0973-7723', '0970-9134'}) v28 n3
    [11:31:58] INFO     10.1007/s12055-012-0157-1 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/0973-7723).

---

#### Journal of Pathogens
Portico contains: Journal of Pathogens (issn: 2090-3057) (eissn: 2090-3065) (v2011 - v. 2011 (null), 2012 - v. 2012 (null), 2013 - v. 2013 (null), 2014 - v. 2014 (null), 2015 - v. 2015 (null), 2016 - v. 2016 (null), 2017 - v. 2017 (null), 2018 - v. 2018 (null), 2019 - v. 2019 (null), 2020 - v. 2020 (null), 2021 - v. 2021 (null), 2022 - v. 2022 (null))

DOI 1:

    [11:35:25] INFO     Checking 10.1155/2015/789265: ['Journal of Pathogens'] ({'2090-3065', '2090-3057'}) v2015 nNone
    [11:35:27] INFO     10.1155/2015/789265 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:36:45] INFO     Checking 10.1155/2013/534342: ['Journal of Pathogens'] ({'2090-3065', '2090-3057'}) v2013 nNone
    [11:36:48] INFO     10.1155/2013/534342 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved (in progress): in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2090-3065).

---

#### BSHM Bulletin: Journal of the British Society for the History of Mathematics
Portico contains: BSHM Bulletin: Journal of the British Society for the History of Mathematics (issn: 1749-8430) (eissn: 1749-8341) (v1997 - v. 12 (1-3), 1998 - v. 13 (1-3), 1999 - v. 14 (1-2), 2000 - v. 15 (1-2), 2001 - v. 16 (1-2), 2002/2003 - v. 17 (1-3), 2003 - v. 18 (1-2), 2004 - v. 19 (1-3), 2005 - v. 20 (1-3), 2006 - v. 21 (1-3), 2007 - v. 22 (1-3), 2008 - v. 23 (1-3), 2009 - v. 24 (1-3), 2010 - v. 25 (1-3), 2011 - v. 26 (1-3), 2012 - v. 27 (1-3), 2013 - v. 28 (1-3), 2014 - v. 29 (1-3), 2015 - v. 30 (1-3), 2016 - v. 31 (1-3), 2017 - v. 32 (1-3), 2018 - v. 33 (1-3))

DOI 1:

    [11:43:53] INFO     Checking 10.1080/17498430.2017.1326216: ['BSHM Bulletin: Journal of the British Society for the History of Mathematics'] ({'1749-8341', '1749-8430'}) v32 n3
    [11:43:56] INFO     10.1080/17498430.2017.1326216 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [11:45:03] INFO     Checking 10.1080/17498430600964433: ['BSHM Bulletin: Journal of the British Society for the History of Mathematics'] ({'1749-8341', '1749-8430'}) v21 n3
    [11:45:05] INFO     10.1080/17498430600964433 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1749-8341).

---

#### Journal of Molluscan Studies
Portico contains: Journal of Molluscan Studies (issn: 0260-1230) (eissn: 1464-3766) (v1986 - v. 52 (1-2), 1987 - v. 53 (1-3), 1988 - v. 54 (1-4), 1989 - v. 55 (1-4), 1990 - v. 56 (1-4), 1991 - v. 57 (1-4), 1992 - v. 58 (1-4), 1993 - v. 59 (1-4), 1994 - v. 60 (1-4), 1995 - v. 61 (1-4), 1996 - v. 62 (1-4), 1997 - v. 63 (1-4), 1998 - v. 64 (1-4), 1999 - v. 65 (1-4), 2000 - v. 66 (1-4), 2001 - v. 67 (1-4), 2002 - v. 68 (1-4), 2003 - v. 69 (1-4), 2004 - v. 70 (1-4), 2005 - v. 71 (1-4), 2006 - v. 72 (1-4), 2007 - v. 73 (1-4), 2007/2008 - v. 74 (1-4), 2008/2009 - v. 75 (1-4), 2010 - v. 76 (1-4), 2010/2011 - v. 77 (1-4), 2012 - v. 78 (1-4), 2013 - v. 79 (1-4), 2013/2014 - v. 80 (1-5), 2014/2015 - v. 81 (1-4), 2015/2016 - v. 82 (1-4), 2017 - v. 83 (1-4), 2018 - v. 84 (1-4), 2019 - v. 85 (1-4), 2020 - v. 86 (1-4), 2021 - v. 87 (1-4), 2022 - v. 88 (1-4), 2023 - v. 89 (1))

DOI 1:

    [11:48:14] INFO     Checking 10.1093/mollus/65.1.33: ['Journal of Molluscan Studies'] ({'0260-1230', '1464-3766'}) v65 n1
    [11:48:17] INFO     10.1093/mollus/65.1.33 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [11:49:33] INFO     Checking 10.1093/mollus/eyn034: ['Journal of Molluscan Studies'] ({'1464-3766', '0260-1230'}) v75 n1
    [11:49:36] INFO     10.1093/mollus/eyn034 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1464-3766).

---

#### Sustentabilidade em Debate
Portico does not contain: Sustentabilidade em Debate (issn: 2177-7675) (eissn: 2179-9067)

DOI 1:

    [11:57:21] INFO     Checking 10.18472/sustdeb.v11n2.2020.33466: ['Sustentabilidade em Debate'] ({'2177-7675', '2179-9067'}) v11 n2
    [11:57:24] INFO     10.18472/SustDeb.v11n2.2020.33466 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [11:58:33] INFO     Checking 10.18472/sustdeb.v13n3.2022.45523: ['Sustainability in Debate'] ({'2179-9067', '2177-7675'}) v13 n3
    [11:58:36] INFO     10.18472/SustDeb.v13n3.2022.45523 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2179-9067).

---

#### Fisioterapia em Movimento
Portico does not contain: Fisioterapia em Movimento (issn: 0103-5150) (eissn: 1980-5918)

DOI 1:

    [12:04:44] INFO     Checking 10.1590/1980-5918.029.003.ao01: ['Fisioterapia em Movimento'] ({'0103-5150'}) v29 n3
    [12:04:46] INFO     10.1590/1980-5918.029.003.AO01 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:06:05] INFO     Checking 10.1590/0103-5150.027.002.ao03: ['Fisioterapia em Movimento'] ({'0103-5150'}) v27 n2
    [12:06:06] INFO     10.1590/0103-5150.027.002.AO03 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1980-5918).

---

#### Comunicação & Sociedade
Portico does not contain: Comunicação & Sociedade (issn: 0101-2657) (eissn: 2175-7755)

DOI 1:

    [12:09:27] INFO     Checking 10.15603/2175-7755/cs.v40n2p139-164: ['Comunicação &amp; Sociedade'] ({'2175-7755', '0101-2657'}) v40 n2
    [12:09:30] INFO     10.15603/2175-7755/cs.v40n2p139-164 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:12:03] INFO     Checking 10.15603/2175-7755/cs.v34n1p83-108: ['Comunicação &amp; Sociedade'] ({'2175-7755'}) v34 n1
    [12:12:05] INFO     10.15603/2175-7755/cs.v34n1p83-108 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2175-7755).

---

#### Brazilian Journal of Latin American Studies (Cadernos PROLAM) 
Portico does not contain: Brazilian Journal of Latin American Studies (Cadernos PROLAM) (issn: ) (eissn: 1676-6288)

DOI 1:

    [12:16:33] INFO     Checking 10.11606/issn.1676-6288.prolam.2019.164124: ['Cadernos PROLAM/USP'] ({'1676-6288'}) v18 n35
    [12:16:35] INFO     10.11606/issn.1676-6288.prolam.2019.164124 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:18:55] INFO     Checking 10.11606/issn.1676-6288.prolam.2022.193244: ['Cadernos PROLAM/USP'] ({'1676-6288'}) v21 n42
    [12:18:57] INFO     10.11606/issn.1676-6288.prolam.2022.193244 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records do not correspond on all archives](https://portal.issn.org/resource/ISSN/1676-6288). (Our source for PKP PLN is more recent.)

---

#### Contexto & Educação
Portico does not contain: Revista Contexto & Educação (issn: 0102-8758) (eissn: 2179-1309)

DOI 1:

    [12:26:35] INFO     Checking 10.21527/2179-1309.2018.106.56-71: ['Revista Contexto &amp; Educação'] ({'2179-1309', '0102-8758'}) v33 n106
    [12:26:38] INFO     10.21527/2179-1309.2018.106.56-71 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [12:28:00] INFO     Checking 10.21527/2179-1309.2023.120.12481: ['Revista Contexto &amp; Educação'] ({'0102-8758', '2179-1309'}) v38 n120
    [12:28:02] INFO     10.21527/2179-1309.2023.120.12481 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2179-1309).

---

### Scholars Portal

#### Journal of Business Ethics
OCUL Scholars Portal contains: Journal of Business Ethics (issn: 0167-4544) (v180(1))

DOI 1:

    [16:00:18] INFO     Checking 10.1007/s10551-019-04310-9: ['Journal of Business Ethics'] ({'0167-4544', '1573-0697'}) v169 n3
    [16:00:20] INFO     10.1007/s10551-019-04310-9 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [16:01:52] INFO     Checking 10.1007/s10551-006-9126-1: ['Journal of Business Ethics'] ({'1573-0697', '0167-4544'}) v74 n1
    [16:01:54] INFO     10.1007/s10551-006-9126-1 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1573-0697).

---

#### Head & Neck
OCUL Scholars Portal contains: Head & Neck (issn: 1043-3074) (v37(S1))

DOI 1:

    [16:09:53] INFO     Checking 10.1002/hed.27146: ['Head &amp; Neck'] ({'1043-3074', '1097-0347'}) v44 n11
    [16:09:56] INFO     10.1002/hed.27146 correctly asserts preservation in Portico.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [16:22:03] INFO     Checking 10.1002/(sici)1097-0347(199705)19:3<188::aid-hed4>3.0.co;2-z: ['Head &amp; Neck'] ({'1097-0347', '1043-3074'}) v19 n3
    [16:22:06] INFO     10.1002/(SICI)1097-0347(199705)19:3%3C188::AID-HED4%3E3.0.CO;2-Z asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1097-0347).

---

#### Keats-Shelley Review
OCUL Scholars Portal contains: Keats-Shelley Review (issn: 0952-4142) (v28(1))

DOI 1:

    [16:26:36] INFO     Checking 10.1080/09524142.2019.1611267: ['The Keats-Shelley Review'] ({'0952-4142', '2042-1362'}) v33 n1
    [16:26:38] INFO     10.1080/09524142.2019.1611267 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [16:28:48] INFO     Checking 10.1179/ksr.1993.8.1.27: ['The Keats-Shelley Review'] ({'2042-1362', '0952-4142'}) v8 n1
    [16:28:51] INFO     10.1179/ksr.1993.8.1.27 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved (in progress): in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2042-1362).

---

#### The Ramanujan journal
OCUL Scholars Portal contains: The Ramanujan Journal (issn: 1382-4090) (v23(1-3))

DOI 1:

    [16:33:04] INFO     Checking 10.1007/s11139-018-9993-y: ['The Ramanujan Journal'] ({'1572-9303', '1382-4090'}) v46 n2
    [16:33:06] INFO     10.1007/s11139-018-9993-y asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [16:36:10] INFO     Checking 10.1007/s11139-010-9221-x: ['The Ramanujan Journal'] ({'1572-9303', '1382-4090'}) v24 n1
    [16:36:12] INFO     10.1007/s11139-010-9221-x asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1572-9303).

---

#### Journal of Algorithms
OCUL Scholars Portal contains: Journal of Algorithms (issn: 0196-6774) (v28(1))

DOI 1:

    [16:42:20] INFO     Checking 10.1016/s0196-6774(02)00249-3: ['Journal of Algorithms'] ({'0196-6774'}) v45 n2
    [16:42:22] INFO     10.1016/S0196-6774(02)00249-3 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

DOI 2:

    [16:43:47] INFO     Checking 10.1006/jagm.1996.0046: ['Journal of Algorithms'] ({'0196-6774'}) v21 n2
    [16:43:49] INFO     10.1006/jagm.1996.0046 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1090-2678).

---

#### Brazilian Journal of Psychiatry
OCUL Scholars Portal does not contain: Brazilian Journal of Psychiatry (issn: 1516-4446) (eissn: 1809-452X)

DOI 1:

    [16:47:51] INFO     Checking 10.1590/1516-4446-2013-1221: ['Revista Brasileira de Psiquiatria'] ({'1809-452X', '1516-4446'}) v36 n2
    [16:47:54] INFO     10.1590/1516-4446-2013-1221 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [16:48:59] INFO     Checking 10.1590/s1516-44462004000300003: ['Revista Brasileira de Psiquiatria'] ({'1516-4446'}) v26 n3
    [16:49:01] INFO     10.1590/S1516-44462004000300003 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1809-452X).

---

#### Hematology, Transfusion and Cell Therapy
OCUL Scholars Portal does not contain: Hematology, Transfusion and Cell Therapy (issn: 2531-1379) (eissn: 2531-1387)

DOI 1:

    [16:58:06] INFO     Checking 10.1016/j.htct.2020.04.011: ['Hematology, Transfusion and Cell Therapy'] ({'2531-1379'}) v43 n4
    [16:58:08] INFO     10.1016/j.htct.2020.04.011 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [16:58:53] INFO     Checking 10.1016/j.htct.2018.05.014: ['Hematology, Transfusion and Cell Therapy'] ({'2531-1379'}) v41 n1
    [16:58:55] INFO     10.1016/j.htct.2018.05.014 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2531-1387).

---

#### Revista de Sociologia e Política
OCUL Scholars Portal does not contain: Revista de Sociologia e PolÃ­tica (issn: 0104-4478) (eissn: 1678-9873)

DOI 1:

    [17:02:24] INFO     Checking 10.1590/1678-987318266601: ['Revista de Sociologia e Política'] ({'0104-4478', '1678-9873'}) v26 n66
    [17:02:27] INFO     10.1590/1678-987318266601 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [17:03:08] INFO     Checking 10.1590/s0104-44782013000200007: ['Revista de Sociologia e Política'] ({'0104-4478'}) v21 n46
    [17:03:09] INFO     10.1590/S0104-44782013000200007 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1678-9873).

---

#### Em Questão
OCUL Scholars Portal does not contain: em questÃ£o (issn: 1807-8893) (eissn: 1808-5245)

DOI 1:

    [17:06:46] INFO     Checking 10.19132/1808-5245284.123710: ['Em Questão'] ({'1807-8893', '1808-5245'}) vNone nNone
    [17:06:48] INFO     10.19132/1808-5245284.123710 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [17:07:51] INFO     Checking 10.19132/1808-5245222.36-59: ['Em Questão'] ({'1808-5245', '1807-8893'}) v22 n2
    [17:07:54] INFO     10.19132/1808-5245222.36-59 asserts no preservation information.
               INFO     Preserved (in progress): in Cariniana
               INFO     Not preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Not preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/1808-5245).

---

#### Brazilian Journal of Anesthesiology
OCUL Scholars Portal does not contain: Brazilian Journal of Anesthesiology (issn: 2352-2291) (eissn: 2352-2291)

DOI 1:

    [17:10:55] INFO     Checking 10.1016/j.bjane.2022.12.001: ['Brazilian Journal of Anesthesiology (English Edition)'] ({'0104-0014'}) v73 n1
    [17:10:56] INFO     10.1016/j.bjane.2022.12.001 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

DOI 2:

    [17:12:02] INFO     Checking 10.1016/j.bjane.2021.10.018: ['Brazilian Journal of Anesthesiology (English Edition)'] ({'0104-0014'}) v73 n1
    [17:12:04] INFO     10.1016/j.bjane.2021.10.018 asserts no preservation information.
               INFO     Not preserved: in Cariniana
               INFO     Preserved: in CLOCKSS
               INFO     Not preserved: in HathiTrust
               INFO     Not preserved: in LOCKSS
               INFO     Not preserved: in PKP PLN
               INFO     Preserved: in Portico
               INFO     Not preserved: in OCUL Scholars Portal

Keepers: [records correspond](https://portal.issn.org/resource/ISSN/2352-2291).

---

## Failing Edge Cases (v <0.0.43)
Incorrect or incomplete metadata at a preservation archive or lodged in a DOI can cause bad results in database versions below 0.0.43.

For example, the title _IET Computer Vision_ has ISSN 1751-9632 and eISSN 1751-9640.

OCUL Scholars Portal preserves 'Computer Vision, IET' (issn: 1751-9632) (v11(1)), but only under the ISSN, not under the eISSN.

Resolving the DOI https://doi.org/10.1049/iet-cvi.2019.0461 gives: ['IET Computer Vision'] (['1751-9640', '1751-9640']) v14 n2.

Essentially, instead of registering both the ISSN and eISSN, the registrant has accidentally registered just the eISSN twice. This means that we never see a hit against the database entry for the 1751-9632 ISSN, which is what the preservation system has logged.

In this instance, we also cannot resolve the item using the container title because the preservation archive has normalized to 'Computer Vision, IET' while the ISSN record and DOI record 'IET computer vision'. 

Database v 0.0.43 gives the ability to import [ISSNL to ISSN mappings](https://archive.org/details/issn_issnl_mappings) and then to expand the list of ISSNs associated with a title. To override this and induce the legacy behaviour, use the CLI --no-issn switch.