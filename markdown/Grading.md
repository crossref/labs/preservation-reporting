# Grading System
We employ an internal grading system to rate members based on the percentage of their content that is digitally preserved.

Gold members are those that have 75% of their content digitally preserved in three or more recognised archives.

Silver members are those that have 50% of their content digitally preserved in two or more recognised archives.

Bronze members are those that have 25% of their content digitally preserved in one or more recognised archives.

Unclassified members are those that do not meet any of the above criteria.