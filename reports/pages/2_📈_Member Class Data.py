import pandas as pd
import streamlit as st
import altair as alt

# note: this import sets up the Django ORM
import pages.common.environment as environment
import pages.common.constants as constants
import pages.common.data_sources as data_sources

from preservationdatabase import constants as pd_constants
from caseconverter import kebabcase


def header() -> None:
    """
    Writes the page header.
    :return: None
    """
    environment.page_header()
    st.header("Member Class Data")


def layout_page():
    s3, s3_client = environment.get_s3()

    archives = [archive.name() for archive in pd_constants.archives.values()]

    header()
    st.subheader("Gold Members")

    for member in report["gold-member-ids"]:
        print_member(member)

    st.subheader("Silver Members")

    for member in report["silver-member-ids"]:
        print_member(member)

    st.subheader("Bronze Members")

    for member in report["bronze-member-ids"]:
        print_member(member)


def print_member(member):
    # extract the member id from the member (it's in brackets at the end)
    member_id = member.split(" ")[-1].strip("()")

    # extract the member name, which is everything except the number in
    # brackets at the end
    member_name = member.replace(f" ({member_id})", "")

    member_url = "Member_Data?members={}".format(f"{member_id} ({member_name})")

    st.markdown(
        f'<a href="{member_url}" target="_self">{member}</a>',
        unsafe_allow_html=True,
    )


s3, s3client = environment.get_s3()

report = data_sources.get_report(
    s3client=s3client,
    report_bucket=constants.REPORT_BUCKET,
    report_path=constants.REPORT_PATH,
    report_name="preservation.json",
)

layout_page()
