import json

import pandas as pd
import streamlit as st
import altair as alt
import streamlit_permalink as stp

# note: this import sets up the Django ORM
import pages.common.environment as environment
import pages.common.constants as constants
import pages.common.data_sources as data_sources

from preservationdatabase import constants as pd_constants
from caseconverter import kebabcase


def output_data(final_dataframe) -> None:
    """
    Write the dataframe to the page
    :param final_dataframe: the dataframe to write
    :return: None
    """
    st.header("Dataset")
    st.write(final_dataframe)


def member_statement(grades, active, member_fee_classes, api_data) -> None:
    """
    Create a member statement for each selected member
    :param grades: the grades for each member
    :param active: the active status for each member
    :param member_fee_classes: the fee classes for each member
    :param api_data: the API data for each member
    :return: None
    """
    for key, val in grades.items():
        st.subheader(key)

        if "unpreserved-count" not in val:
            val["unpreserved-count"] = 0

        percent_unpreserved = str(
            round(val["unpreserved-count"] / val["sample-count"] * 100, 2)
        )

        percent_in_one_archive = str(
            round(
                val["preserved-in-one-archive"] / val["sample-count"] * 100, 2
            )
        )

        percent_in_two_archives = str(
            round(
                val["preserved-in-two-archives"] / val["sample-count"] * 100, 2
            )
        )

        percent_in_three_or_more_archives = str(
            round(
                val["preserved-in-three-or-more-archives"]
                / val["sample-count"]
                * 100,
                2,
            )
        )

        st.header("Member Grades and Information")

        if "excluded-non-journal" not in val:
            val["excluded-non-journal"] = 0

        if "unpreserved-count" not in val:
            val["unpreserved-count"] = 0

        st.write(
            f"This member had {val['sample-count']} works in the "
            f"'{val['about']['sample-file']}' sample. Of those, "
            f"{val['excluded-non-journal']} were excluded because they were "
            f"not journal articles. A further {val['excluded-current-year']} "
            f"were excluded for being too recent. In the remaining "
            f"{int(val['sample-count']) - int(val['excluded-non-journal']) - int(val['excluded-current-year'])} "
            f"works, we found {val['unpreserved-count']} "
            f"unpreserved works ({percent_unpreserved}%), "
            f"{val['preserved-in-one-archive']} works preserved in "
            f"just one archive "
            f"({percent_in_one_archive}%), {val['preserved-in-two-archives']} "
            f"works preserved in two archives "
            f"({percent_in_two_archives}%), "
            f"and {val['preserved-in-three-or-more-archives']} works preserved "
            f"in three or more archives ({percent_in_three_or_more_archives}%)."
            f" As a result, this member was scored: "
            f"'[{val['member-grade']}](/Grading_System)'."
        )

        st.write(
            f"This member is classified in the '{member_fee_classes[key]}' "
            f"fee band."
        )

        st.write(f"This member is based in {api_data[key]['location']}.")

        if active[key]:
            st.write("This member is active.")
        else:
            st.write("This member is inactive.")


def member_map(grades, api_data) -> None:
    """
    Show a map of selected members
    :param grades: the grades for members
    :param api_data: the API data for each member
    :return: None
    """
    frames = []

    for key, val in grades.items():
        location = data_sources.geocode(api_data[key]["location"])

        if location:
            frames.append(
                pd.DataFrame(
                    {
                        "latitude": [location.latitude],
                        "longitude": [location.longitude],
                    }
                )
            )

    geo_spots = pd.concat(frames)

    st.header("Geographical Spread")
    st.map(geo_spots)


def percent_in_each_archive_bar_chart(final_dataframe) -> None:
    """
    Show the percent of a member's output in each archive
    :param final_dataframe: the concatenated dataframe
    :return: None
    """
    st.header("Percentage Preserved In Each Archive")

    chart_two = (
        alt.Chart(final_dataframe)
        .transform_joinaggregate(
            TotalPreserved="sum(preserved-items)", groupby=["Member"]
        )
        .transform_calculate(
            PercentagePreserved="(datum['preserved-items'] / "
            "datum.TotalPreserved) * 100"
        )
        .mark_bar()
        .encode(
            x="Member:N",
            y="PercentagePreserved:Q",
            column="Archive:N",
            color="Member:N",
        )
        .interactive()
    ).configure_range(category=alt.RangeScheme(constants.CROSSREF_COLORS))

    st.altair_chart(chart_two, theme="streamlit", use_container_width=False)


def absolute_preservation_bar_chart(final_dataframe) -> None:
    """
    Show absolute preservation statistics
    :param final_dataframe: the concatenated dataframe
    :return: None
    """
    chart = (
        alt.Chart(final_dataframe)
        .mark_bar()
        .encode(
            x="Member:N",
            y="preserved-items:Q",
            column="Archive:N",
            color="Member:N",
        )
        .interactive()
    ).configure_range(category=alt.RangeScheme(constants.CROSSREF_COLORS))

    st.header("Absolute Preservation Numbers")
    st.altair_chart(chart, theme="streamlit", use_container_width=False)


def populate_member_data(options, archives, s3client):
    """
    Calculate the fields needed for each member
    :param options: the list of members from the streamlit select field
    :param archives: the list of archives
    :param s3client: an AWS s3client object
    :return: a 5-tuple of member_info, grades, member_fee_classes, active, and api_data
    """
    member_info = []

    grades = {}
    member_fee_classes = {}
    active = {}
    api_data = {}
    member_preserved = {}
    member_unpreserved = {}

    for member in options:
        member_id = member.split(" ")[0]

        preservations = data_sources.get_member_annotation(
            s3client=None,
            annotations_bucket=constants.ANNOTATION_BUCKET,
            annotations_path=constants.ANNOTATION_PATH,
            member_id=member_id,
            annotation_name="preservation.json",
        )

        grades[member] = preservations

        member_data = data_sources.get_member_annotation(
            s3client=s3client,
            member_id=member_id,
            annotations_bucket=constants.ANNOTATION_BUCKET,
            annotations_path=constants.ANNOTATION_PATH,
            annotation_name="member-profile.json",
        )

        try:
            member_report = data_sources.get_report(
                s3client=s3client,
                report_name="preservation-report.json",
                report_path=f"reports/members/{member_id}",
                report_bucket=constants.REPORT_BUCKET,
            )

            member_preserved[member] = member_report

            unpreserved_member_report = data_sources.get_report(
                s3client=s3client,
                report_name="unpreserved.json",
                report_path=f"reports/members/{member_id}",
                report_bucket=constants.REPORT_BUCKET,
            )

            member_unpreserved[member] = unpreserved_member_report
        except:
            ...

        annual_fee = int(member_data["annual-fee"])
        member_band = None

        for key, val in constants.MEMBER_TIERS.items():
            if annual_fee <= int(val):
                # put this member in this band
                member_band = key
                break

            if member_band:
                break

        member_fee_classes[member] = member_band

        active[member] = (
            member_data["inactive"] == "No" or member_data["inactive"] == ""
        )

        member_api_data = data_sources.get_member_api_info(member_id=member_id)
        api_data[member] = member_api_data

        for archive in archives:
            member_info.append(
                pd.DataFrame(
                    {
                        "Archive": [archive],
                        "Member": [member],
                        "preserved-items": [
                            int(preservations[normalize_json_key(archive)])
                        ],
                    }
                )
            )

    return (
        member_info,
        grades,
        member_fee_classes,
        active,
        api_data,
        member_preserved,
        member_unpreserved,
    )


def normalize_json_key(key: str) -> str:
    """
    Normalize keys to kebab case
    :param key: the key to normalize
    :return: the normalized key
    """
    # while it seems overkill to have this as a separate function,
    # it's useful in other contexts, too, to have it on its own
    return kebabcase(key)


def concat_dataframes(member_info) -> pd.DataFrame:
    """
    Concatenates the specified dataframes
    :param member_info: a list of dataframes
    :return: a pandas Dataframe
    """
    return pd.concat(
        member_info,
    )


def members_dropdown() -> list:
    """
    Writes the list of Crossref members to a dropdown
    :return: a streamlit multiselect object
    """
    members = data_sources.get_all_members_api()

    output = []

    for member, data in members.items():
        output.append(f"{member} ({data})")

    options = stp.multiselect(
        "Please select some Crossref members", output, url_key="members"
    )

    return options


def header() -> None:
    """
    Writes the page header.
    :return: None
    """
    environment.page_header()
    st.header("Member Data")


def member_preserved_data(data, heading, api_data) -> None:
    """
    Create a member statement for each selected member
    :param data: the data for each member
    :param heading: the heading for the member statement
    :param api_data: the API data for each member
    :return: None
    """
    for key, val in data.items():
        st.subheader(f"{heading} for {key}")
        st.write(val)


def layout_page():
    s3, s3_client = environment.get_s3()

    archives = [archive.name() for archive in pd_constants.archives.values()]

    header()
    options = members_dropdown()

    (
        member_info,
        grades,
        member_fee_classes,
        active,
        api_data,
        member_preserved,
        member_unpreserved,
    ) = populate_member_data(options, archives=archives, s3client=s3_client)

    # if we have any members selected
    if len(member_info) > 0:
        final_dataframe = concat_dataframes(member_info)

        absolute_preservation_bar_chart(final_dataframe)
        percent_in_each_archive_bar_chart(final_dataframe)

        member_statement(
            grades=grades,
            active=active,
            member_fee_classes=member_fee_classes,
            api_data=api_data,
        )

        member_preserved_data(
            data=member_preserved,
            heading="Preservation Data",
            api_data=api_data,
        )

        member_preserved_data(
            data=member_unpreserved,
            heading="Unpreserved Data",
            api_data=api_data,
        )

        output_data(final_dataframe)


layout_page()
