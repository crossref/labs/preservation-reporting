import os

import pages.common.environment

os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
environment.setup_environment(code_bucket=None, download_settings=False)
