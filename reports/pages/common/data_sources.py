import json
from datetime import timedelta
from io import StringIO
from pathlib import Path

import boto3
import crossref.restful
import geopy
import streamlit as st
from crossref.restful import Etiquette, Members
from crossref.restful import MaxOffsetError
from django.conf import settings
from geopy.geocoders import Nominatim

my_etiquette = Etiquette(
    "Preservation Reports", "0.01", "https://eve.gd", "meve@crossref.org"
)


@st.cache_data(ttl=timedelta(weeks=1))
def get_all_members_api() -> dict:
    """
    Retrieves the list of members from either disk or the API so we can assign names
    :return: a dictionary of member information
    """

    # first, we check to see if we have gbilder's daily dump
    dump_file = Path("data/members.json")

    if not dump_file.exists():
        member_object = _build_member_object()

        members = member_object.all()
    else:
        with open(dump_file, "r") as f:
            members = json.load(f)

    output = {}

    try:
        for member in members:
            output[member["id"]] = member["primary-name"]
    except MaxOffsetError:
        pass

    return output


def get_member_api_info(member_id) -> dict | None:
    """]
    Fetch member details from the Crossref REST API
    :param member_id: the member ID
    :return: a dixtionary of member info or Nome
    """
    members = _build_member_object()

    return members.member(member_id=member_id)


def _build_member_object() -> crossref.restful.Members:
    """
    Build a Crossref API object for retrieving members
    :return: a crossref.restful.Members object
    """
    plus_token = settings.CROSSREF_PLUS_TOKEN

    if plus_token:
        members = Members(
            etiquette=my_etiquette,
            crossref_plus_token=plus_token,
        )
    else:
        members = Members(
            etiquette=my_etiquette,
        )

    return members


def get_report(s3client, report_bucket, report_path, report_name) -> dict:
    """
    Fetch the overall report from S3
    :param s3client: an s3client object
    :param report_bucket: the S3 bucket with the report
    :param report_path: the path to the report
    :param report_name: the name of the report object
    :return: a JSON object of the report
    """
    key = f"{report_path}/{report_name}"

    return get_annotation(
        annotations_bucket=report_bucket, key=key, s3client=s3client
    )


def get_member_annotation(
    s3client, member_id, annotations_bucket, annotations_path, annotation_name
) -> dict:
    """
    Read a member annotation from S3
    :param s3client: the AWS S3 client object to use
    :param member_id: the member ID to process
    :param annotations_bucket: the annotations bucket to read from
    :param annotations_path: the annotation path to read
    :param annotation_name: the annotation name to read
    :return: a JSON annotation
    """
    key = f"{annotations_path}/members/{member_id}/{annotation_name}"

    return get_annotation(
        annotations_bucket=annotations_bucket, key=key, s3client=s3client
    )


def get_annotation(annotations_bucket, key, s3client=None) -> dict:
    """
    Read a JSON annotation from S3
    :param annotations_bucket: the annotations bucket
    :param key: the key to read
    :param s3client: an optional S3 client
    :return:
    """
    if s3client is None:
        s3client = boto3.client("s3")

    data = (
        s3client.get_object(Bucket=annotations_bucket, Key=key)["Body"]
        .read()
        .decode("utf-8")
    )

    with StringIO(data) as json_file:
        output = json.loads(json_file.read())

        return output


@st.cache_data(ttl=timedelta(weeks=4))
def geocode(location_name: str) -> geopy.location.Location | None:
    """
    Translate a string name into geocoded coordinates
    :param location_name: the location name
    :return: a Location object
    """
    # Initialize Nominatim API
    geolocator = Nominatim(user_agent="Preservation Database")

    location = geolocator.geocode(location_name)

    if not location:
        location = geolocator.geocode(location_name.split(",")[-1])

    return location


def load_file(fn) -> str:
    """
    Read a file from disk (note: loads whole file into memory)
    :param fn: the filename to read
    :return: the file contents
    """
    with open(f"markdown/{fn}") as f:
        return f.read()
