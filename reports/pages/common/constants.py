import matplotlib.colors as mcolors

SAMPLES_BUCKET = "samples-crossref-research"
SAMPLES_PATH = "members-works/sample-2023-01-21/works/"

ANNOTATION_BUCKET = "outputs.research.crossref.org"
ANNOTATION_PATH = "annotations"
ANNOTATION_FILENAME = "preservation.json"

REPORT_BUCKET = "outputs.research.crossref.org"
REPORT_PATH = "reports"
REPORT_FILENAME = "preservation.json"

CODE_BUCKET = "airflow-crossref-research-annotation"

PARALLEL_JOBS = 5

MEMBER_TIER_FEE_NAMES = [
    "<USD 1 million",
    "USD 1 million - USD 5 million",
    "USD 5 million - USD 10 million",
    "USD 10 million - USD 25 million",
    "USD 25 million - USD 50 million",
    "USD 50 million - USD 100 million",
    "USD 100 million - USD 200 million",
    "USD 200 million - USD 500 million",
    ">USD 500 million",
]

MEMBER_TIERS = {
    "<USD 1 million": 275,
    "USD 1 million - USD 5 million": 550,
    "USD 5 million - USD 10 million": 1650,
    "USD 10 million - USD 25 million": 3900,
    "USD 25 million - USD 50 million": 8300,
    "USD 50 million - USD 100 million": 14000,
    "USD 100 million - USD 200 million": 22000,
    "USD 200 million - USD 500 million": 33000,
    ">USD 500 million": 50000,
}

CROSSREF_COLORS = [
    "#ef3340",
    "#3eb1c8",
    "#d8d2c4",
    "#ffc72c",
    "#4f5858",
    "#00ab84",
    "#ffa300",
    "#a6192e",
    "#a39382",
    "#005f83",
]

SIZE_BRACKETS = [
    "1-10",
    "11-100",
    "101-500",
    "501-1000",
    "1001-10000",
    "10001-100000",
    "100001-1000,000",
    "1,000,001",
]

COLORS = [
    mcolors.CSS4_COLORS["lightcyan"],
    mcolors.CSS4_COLORS["tan"],
    mcolors.CSS4_COLORS["silver"],
    mcolors.CSS4_COLORS["gold"],
]
