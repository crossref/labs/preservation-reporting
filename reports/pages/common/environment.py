import inspect
import logging
import os
import sys
from pathlib import Path
from typing import Any

import boto3
from rich.console import Console
from rich.logging import RichHandler

import streamlit as st


def get_s3() -> (boto3.client, Any):
    """
    Get s3 objects.
    :return: a tuple of boto3 client and s3 object
    """
    s3client = boto3.client("s3")
    session = boto3.Session()
    s3 = session.resource("s3")

    return s3, s3client


def setup_environment(code_bucket, download_settings=True) -> None:
    """
    Initialize the Django ORM environment.
    :param code_bucket: the S3 bucket where the code and settings are stored.
    :param download_settings: whether to download the settings file from S3.
    :return: None
    """
    logging.basicConfig(
        level=logging.INFO,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler(console=Console(stderr=True))],
    )
    log = logging.getLogger("rich")
    log.setLevel(logging.INFO)

    current_directory = os.path.dirname(
        os.path.abspath(inspect.getfile(inspect.currentframe()))
    )

    if download_settings:
        settings_file = Path(current_directory) / "downloaded_settings.py"

        logging.info(
            "Settings file downloading to: " "{}".format(settings_file)
        )

        s3client = boto3.client("s3")
        s3client.download_file(code_bucket, "settings.py", str(settings_file))

    if code_bucket:
        sys.path.append(current_directory)
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "downloaded_settings")

    import django

    django.setup()


def page_header():
    """
    Writes the page header.
    :return: None
    """
    st.title("The Vault")
    st.title("Crossref Labs Preservation Report")
