import matplotlib.colors as mcolors
import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from plotly.subplots import make_subplots

from pages.common import constants, data_sources, environment


def build_class_data() -> list[pd.DataFrame]:
    """
    Builds a list of dataframes grouping members by fee tier.
    :return: a list of dataframes grouped by fee tier
    """

    band_totals = {}

    for band in constants.MEMBER_TIER_FEE_NAMES:
        band_totals[band] = (
            report["gold-members-by-fee"][band]
            + report["silver-members-by-fee"][band]
            + report["bronze-members-by-fee"][band]
            + report["unclassified-members-by-fee"][band]
        )

    # this will only work in Python versions that have ordered dictionaries
    # underneath standard dictionaries (Python 3.7+)

    gold_data = pd.DataFrame(
        {
            "Member Classes": constants.MEMBER_TIER_FEE_NAMES,
            "Members": list(report["gold-members-by-fee"].values()),
            "Category": "Gold",
            "Order": 3,
        }
    )

    silver_data = pd.DataFrame(
        {
            "Member Classes": constants.MEMBER_TIER_FEE_NAMES,
            "Members": list(report["silver-members-by-fee"].values()),
            "Category": "Silver",
            "Order": 2,
        }
    )

    bronze_data = pd.DataFrame(
        {
            "Member Classes": constants.MEMBER_TIER_FEE_NAMES,
            "Members": list(report["bronze-members-by-fee"].values()),
            "Category": "Bronze",
            "Order": 1,
        }
    )

    unclassified_data = pd.DataFrame(
        {
            "Member Classes": constants.MEMBER_TIER_FEE_NAMES,
            "Members": list(report["unclassified-members-by-fee"].values()),
            "Category": "Unclassified",
            "Order": 0,
        }
    )

    return [gold_data, silver_data, bronze_data, unclassified_data]


def build_country_data() -> list[pd.DataFrame]:
    """
    Builds a list of dataframes grouping members by country.
    :return: a list of dataframes grouped by country
    """

    band_totals = {}

    overall_countries = set(
        list(report["gold-members-by-country"].keys())
        + list(report["silver-members-by-country"].keys())
        + list(report["bronze-members-by-country"].keys())
        + list(report["unclassified-members-by-country"].keys())
    )

    overall_countries = list(map(str.strip, overall_countries))

    new_gold = {}

    for key, item in report["gold-members-by-country"].items():
        if key in new_gold:
            new_gold[key.strip()] += item
        else:
            new_gold[key.strip()] = item

    report["gold-members-by-country"] = new_gold

    new_silver = {}

    for key, item in report["silver-members-by-country"].items():
        if key in new_silver:
            new_silver[key.strip()] += item
        else:
            new_silver[key.strip()] = item

    report["silver-members-by-country"] = new_silver

    new_bronze = {}

    for key, item in report["bronze-members-by-country"].items():
        if key in new_bronze:
            new_bronze[key.strip()] += item
        else:
            new_bronze[key.strip()] = item

    report["bronze-members-by-country"] = new_bronze

    new_unclassified = {}

    for key, item in report["unclassified-members-by-country"].items():
        if key in new_unclassified:
            new_unclassified[key.strip()] += item
        else:
            new_unclassified[key.strip()] = item

    report["unclassified-members-by-country"] = new_unclassified

    for band in overall_countries:
        band_totals[band] = (
            report["gold-members-by-country"].get(band, 0)
            + report["silver-members-by-country"].get(band, 0)
            + report["bronze-members-by-country"].get(band, 0)
            + report["unclassified-members-by-country"].get(band, 0)
        )

    from collections import OrderedDict

    OrderedDict(sorted(band_totals.items(), key=lambda kv: kv[1], reverse=True))

    # this will only work in Python versions that have ordered dictionaries
    # underneath standard dictionaries (Python 3.7+)

    gold_data_by_country = pd.DataFrame(
        {
            "Country": list(report["gold-members-by-country"].keys()),
            "Members": list(report["gold-members-by-country"].values()),
            "Category": "Gold",
            "Order": 3,
        }
    )

    silver_data_by_country = pd.DataFrame(
        {
            "Country": list(report["silver-members-by-country"].keys()),
            "Members": list(report["silver-members-by-country"].values()),
            "Category": "Silver",
            "Order": 2,
        }
    )

    bronze_data_by_country = pd.DataFrame(
        {
            "Country": list(report["bronze-members-by-country"].keys()),
            "Members": list(report["bronze-members-by-country"].values()),
            "Category": "Bronze",
            "Order": 1,
        }
    )

    unclassified_data_by_country = pd.DataFrame(
        {
            "Country": list(report["unclassified-members-by-country"].keys()),
            "Members": list(report["unclassified-members-by-country"].values()),
            "Category": "Unclassified",
            "Order": 0,
        }
    )

    return [
        gold_data_by_country,
        silver_data_by_country,
        bronze_data_by_country,
        unclassified_data_by_country,
    ]


def build_data_by_size() -> list[pd.DataFrame]:
    """
    Builds a list of dataframes grouping members by size (number of deposits).
    :return: a list of dataframes grouped by size
    :return:
    """
    gold_data_by_size = pd.DataFrame(
        {
            "Number of Deposits": report["gold-members-by-size"].keys(),
            "Members": report["gold-members-by-size"].values(),
            "Category": "Gold",
            "Order": 3,
        }
    )
    silver_data_by_size = pd.DataFrame(
        {
            "Number of Deposits": report["silver-members-by-size"].keys(),
            "Members": report["silver-members-by-size"].values(),
            "Category": "Silver",
            "Order": 2,
        }
    )
    bronze_data_by_size = pd.DataFrame(
        {
            "Number of Deposits": report["bronze-members-by-size"].keys(),
            "Members": report["bronze-members-by-size"].values(),
            "Category": "Bronze",
            "Order": 1,
        }
    )
    unclassified_data_by_size = pd.DataFrame(
        {
            "Number of Deposits": report["unclassified-members-by-size"].keys(),
            "Members": report["unclassified-members-by-size"].values(),
            "Category": "Unclassified",
            "Order": 0,
        }
    )
    return [
        gold_data_by_size,
        silver_data_by_size,
        bronze_data_by_size,
        unclassified_data_by_size,
    ]


def header() -> None:
    """
    Writes the page header.
    :return: None
    """
    environment.page_header()
    st.header("All Members Preservation Grades")


def member_metrics() -> None:
    """
    Writes the member metrics columns.
    :return: None
    """
    col1, col2, col3, col4 = st.columns(4)
    col1.metric(
        "Unclassified Members\r\n\r\n:red[< 25% in 1+ archives]",
        f"{report['unclassified-totals']} "
        f"({report['unclassified-totals'] / member_count * 100:.0f}%)",
    )
    col2.metric(
        "Bronze Members\r\n\r\n:orange[25% in 1+ archives]",
        f"{report['bronze-totals']} "
        f"({report['bronze-totals'] / member_count * 100:.0f}%)",
    )
    col3.metric(
        "Silver Members\r\n\r\n:orange[50% in 2+ archives]",
        f"{report['silver-totals']} "
        f"({report['silver-totals'] / member_count * 100:.0f}%)",
    )
    col4.metric(
        "Gold Members\r\n\r\n:green[75% in 3+ archives]",
        f"{report['gold-totals']} "
        f"({report['gold-totals'] / member_count * 100:.0f}%)",
    )


def item_metrics() -> None:
    """
    Writes the item metrics columns.
    :return: None
    """
    col1_2, col2_2, col3_2, col4_2 = st.columns(4)

    col1_2.metric("Unpreserved Works", report["total-items-unpreserved"])
    col2_2.metric("Preserved Works", report["total-items-preserved"])
    col3_2.metric(
        "Preservation Instances", report["total-preservation-instances"]
    )
    col4_2.metric("Items in Sample", report["total-items-sampled"])


def member_donut() -> None:
    """
    Creates a member donut/pie chart of preservation status.
    :return: None
    """
    # Pie chart, where the slices will be ordered and plotted counter-clockwise:
    labels = (
        f"Gold (75% in 3 or more archives) [{report['gold-totals']} members]",
        f"Silver (50% in 2 archives) [{report['silver-totals']} members]",
        f"Bronze (25% in 1 archive) [{report['bronze-totals']} members]",
        f"Unclassified (no archival status) "
        f"[{report['unclassified-totals']} members]",
    )
    sizes = [
        report["gold-totals"],
        report["silver-totals"],
        report["bronze-totals"],
        report["unclassified-totals"],
    ]

    colors = [
        mcolors.CSS4_COLORS["gold"],
        mcolors.CSS4_COLORS["silver"],
        mcolors.CSS4_COLORS["tan"],
        mcolors.CSS4_COLORS["lightcyan"],
    ]

    # Create subplots: use 'domain' type for Pie subplot
    fig = make_subplots(rows=1, cols=1, specs=[[{"type": "domain"}]])
    fig.add_trace(
        go.Pie(
            labels=labels,
            values=sizes,
            name="Preservation Status",
            marker_colors=colors,
        ),
        1,
        1,
    )

    fig.update_layout(
        legend=dict(font=dict(size=15, color="black")),
        legend_title=dict(font=dict(size=15, color="blue")),
    )

    # Use `hole` to create a donut-like pie chart
    fig.update_traces(hole=0.4, hoverinfo="label+percent+name")

    st.plotly_chart(fig, use_container_width=True)


def preservation_grade_bar_charts() -> None:
    """
    Draws preservation bar charts for fees, country, and deposits.
    :return: None
    """

    import altair as alt

    st.header("Member Preservation Grades")
    st.subheader("Grouped by Fee Band")

    chart = (
        alt.Chart(data_set)
        .mark_bar()
        .encode(
            x=alt.X(
                "sum(Members)",
                stack="normalize",
            ),
            y=alt.Y(
                "Member Classes:N",
                sort=constants.MEMBER_TIER_FEE_NAMES,
                axis=alt.Axis(labelLimit=200),
            ),
            color=alt.Color(
                "Category:N", sort=["Unclassified", "Bronze", "Silver", "Gold"]
            ),
            order=alt.Order("Order:N", sort="ascending"),
        )
        .configure_legend(labelLimit=0)
        .interactive()
    ).configure_range(category=alt.RangeScheme(constants.COLORS))

    st.altair_chart(chart, use_container_width=True)
    st.write(data_set)

    st.subheader("Grouped by Country")

    chart = (
        alt.Chart(data_set_by_country)
        .mark_bar()
        .encode(
            x=alt.X(
                "sum(Members)",
                stack="normalize",
            ),
            y=alt.Y(
                "Country:N",
                axis=alt.Axis(labelLimit=200),
            ),
            color=alt.Color(
                "Category:N", sort=["Unclassified", "Bronze", "Silver", "Gold"]
            ),
            order=alt.Order("Order:N", sort="ascending"),
        )
        .configure_legend(labelLimit=0)
        .interactive()
    ).configure_range(category=alt.RangeScheme(constants.COLORS))

    st.altair_chart(chart, use_container_width=True)
    st.write(data_set_by_country)

    sizes = ["Unknown"] + constants.SIZE_BRACKETS

    st.subheader("Grouped by Number of Deposits")

    chart = (
        alt.Chart(data_set_by_size)
        .mark_bar()
        .encode(
            x=alt.X(
                "sum(Members)",
                stack="normalize",
            ),
            y=alt.Y(
                "Number of Deposits:N",
                axis=alt.Axis(labelLimit=200),
                sort=sizes,
            ),
            color=alt.Color(
                "Category:N", sort=["Unclassified", "Bronze", "Silver", "Gold"]
            ),
            order=alt.Order("Order:N", sort="ascending"),
        )
        .configure_legend(labelLimit=0)
        .interactive()
    ).configure_range(category=alt.RangeScheme(constants.COLORS))

    st.altair_chart(chart, use_container_width=True)
    st.write(data_set_by_size)


def layout_page() -> None:
    """
    Draws the streamlit page.
    :return: None
    """
    st.set_page_config(
        page_title="Overall Report",
        page_icon="👋",
    )

    header()
    member_metrics()
    item_metrics()
    member_donut()
    preservation_grade_bar_charts()


# common variables
s3, s3client = environment.get_s3()

report = data_sources.get_report(
    s3client=s3client,
    report_bucket=constants.REPORT_BUCKET,
    report_path=constants.REPORT_PATH,
    report_name="preservation.json",
)

class_labels = ["Gold", "Silver", "Bronze", "Unclassified"]

# now build the types of preservation status
data_set = build_class_data()
data_set = pd.concat(data_set)
data_set_by_country = build_country_data()
data_set_by_country = pd.concat(data_set_by_country)
data_set_by_size = build_data_by_size()
data_set_by_size = pd.concat(data_set_by_size)

member_count = (
    report["unclassified-totals"]
    + report["bronze-totals"]
    + report["silver-totals"]
    + report["gold-totals"]
)

layout_page()
